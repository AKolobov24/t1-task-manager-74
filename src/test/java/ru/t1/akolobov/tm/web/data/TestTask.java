package ru.t1.akolobov.tm.web.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.web.model.Task;
import ru.t1.akolobov.tm.web.model.User;

import java.util.ArrayList;
import java.util.List;

public final class TestTask {

    @NotNull
    public static Task createTask() {
        return new Task("test-task-1");
    }

    @NotNull
    public static Task createTask(@NotNull final User user) {
        @NotNull Task task = createTask();
        task.setUser(user);
        return task;
    }

    @NotNull
    public static List<Task> createTaskList(@NotNull final User user) {
        @NotNull List<Task> taskList = new ArrayList<>();
        for (int i = 1; i <= 5; i++) {
            @NotNull Task task = new Task("test-task-" + i);
            task.setUser(user);
            taskList.add(task);
        }
        return taskList;
    }

}
