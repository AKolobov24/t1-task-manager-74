<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/_header.jsp" />
<h1>TASK LIST</h1>
<table width="100%" cellpadding="10" border="1" style="border-collapse: collapse;" >
    <tr>
        <th>ID</th>
        <th>NAME</th>
        <th>DESCRIPTION</th>
        <th>STATUS</th>
        <th>START</th>
        <th>FINISH</th>
        <th>PROJECT</th>
        <th>EDIT</th>
        <th>DELETE</th>
    </tr>

    <c:forEach var="task" items = "${tasks}" >
        <tr>
            <td>
                <c:out value="${task.id}" />
            </td>
            <td>
                <c:out value="${task.name}" />
            </td>
            <td>
                <c:out value="${task.description}" />
            </td>
            <td align="center">
                <c:out value="${task.status.displayName}" />
            </td>
            <td>
                <fmt:formatDate pattern="dd.MM.yyyy" value="${task.dateStart}" />
            </td>
            <td>
                <fmt:formatDate pattern="dd.MM.yyyy" value="${task.dateFinish}" />
            </td>
            <td>
                <c:out value="${projects.getOrDefault(task.projectId, '')}" />
            </td>
            <td align="center">
                <a href="/task/edit?id=${task.id}">EDIT</a>
            </td>
            <td align="center">
                <form name="deleteTaskForm" action="/task/delete" method="POST" style="margin: 0px;">
                    <input type="hidden" name="id" value="${task.id}" />
                    <button type="submit">DELETE</button>
                </form>
            </td>
        </tr>
    </c:forEach>
</table>

<form action="/task/create" style="margin-top: 20px;">
    <button>CREATE TASK</button>
</form>

<jsp:include page="../include/_footer.jsp" />