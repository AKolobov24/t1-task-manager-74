package ru.t1.akolobov.tm.web.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.web.enumerated.RoleType;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "tm_role")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class Role {

    @Id
    @NotNull
    private String id = UUID.randomUUID().toString();

    @Nullable
    @ManyToOne
    @JsonIgnore
    private User user;

    @NotNull
    @Enumerated
    private RoleType roleType = RoleType.USER;

    public Role() {
    }

    public Role(@NotNull User user, @NotNull RoleType roleType) {
        this.user = user;
        this.roleType = roleType;
    }

    @NotNull
    public String getId() {
        return id;
    }

    public void setId(@NotNull final String id) {
        this.id = id;
    }

    @Nullable
    public User getUser() {
        return user;
    }

    public void setUser(@NotNull final User user) {
        this.user = user;
    }

    @NotNull
    public RoleType getRoleType() {
        return roleType;
    }

    public void setRoleType(@NotNull final RoleType roleType) {
        this.roleType = roleType;
    }

    @NotNull
    @Override
    public String toString() {
        return roleType.name();
    }

}
