package ru.t1.akolobov.tm.web.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.akolobov.tm.web.api.service.IProjectService;
import ru.t1.akolobov.tm.web.api.service.ITaskService;
import ru.t1.akolobov.tm.web.dto.CustomUser;
import ru.t1.akolobov.tm.web.model.Project;
import ru.t1.akolobov.tm.web.model.Task;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

@Controller
public class TasksController {

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IProjectService projectService;

    @NotNull
    @GetMapping("/tasks")
    @PreAuthorize("isAuthenticated()")
    public ModelAndView index(@AuthenticationPrincipal final CustomUser customUser) {
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        @NotNull final Collection<Task> taskList = taskService.findAllByUserId(customUser.getUser().getId());
        modelAndView.setViewName("task-list");
        modelAndView.addObject("tasks", taskList);
        modelAndView.addObject(
                "projects",
                projectService.findAllById(
                        taskList.stream()
                                .map(Task::getProjectId)
                                .collect(Collectors.toCollection(ArrayList<String>::new))
                ).stream().collect(Collectors.toMap(Project::getId, Project::getName))
        );
        return modelAndView;
    }

}
