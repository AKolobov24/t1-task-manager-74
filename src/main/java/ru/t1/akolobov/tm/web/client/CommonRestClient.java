package ru.t1.akolobov.tm.web.client;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.t1.akolobov.tm.web.model.AbstractModel;

import java.util.Collection;

public interface CommonRestClient<M extends AbstractModel> {

    String BASE_URL = "http://localhost:8080";

    @NotNull
    @GetMapping("findAll")
    Collection<M> findAll();

    @GetMapping("findById/{id}")
    M findById(@PathVariable("id") @NotNull final String id);

    @PostMapping("deleteById/{id}")
    void deleteById(@PathVariable("id") @NotNull final String id);

    @NotNull
    @PostMapping("save")
    M save(@RequestBody M project);

}
