package ru.t1.akolobov.tm.web.enumerated;

import org.jetbrains.annotations.NotNull;

public enum RoleType {

    ADMIN("Administrator"),
    USER("User");

    @NotNull
    private final String name;

    RoleType(@NotNull final String name) {
        this.name = name;
    }

    @NotNull
    public String getName() {
        return name;
    }

}
