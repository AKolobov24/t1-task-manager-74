package ru.t1.akolobov.tm.web.configuration;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;
import ru.t1.akolobov.tm.web.endpoint.AuthSoapEndpointImpl;
import ru.t1.akolobov.tm.web.endpoint.ProjectSoapEndpointImpl;
import ru.t1.akolobov.tm.web.endpoint.TaskSoapEndpointImpl;

@EnableWs
@Configuration
public class WebServiceConfiguration extends WsConfigurerAdapter {

    @Bean
    public ServletRegistrationBean<MessageDispatcherServlet> messageDispatcherServlet(
            ApplicationContext applicationContext
    ) {
        MessageDispatcherServlet servlet = new MessageDispatcherServlet();
        servlet.setApplicationContext(applicationContext);
        servlet.setTransformWsdlLocations(true);
        return new ServletRegistrationBean<>(servlet, "/ws/*");
    }

    @Bean(name = "project")
    public DefaultWsdl11Definition projectWsdl11Definition(XsdSchema projectEndpointSchema) {
        final DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setServiceName("project");
        wsdl11Definition.setPortTypeName(ProjectSoapEndpointImpl.PORT_TYPE_NAME);
        wsdl11Definition.setLocationUri(ProjectSoapEndpointImpl.LOCATION_URI);
        wsdl11Definition.setTargetNamespace(ProjectSoapEndpointImpl.NAMESPACE);
        wsdl11Definition.setSchema(projectEndpointSchema);
        return wsdl11Definition;
    }

    @Bean
    public XsdSchema projectEndpointSchema() {
        return new SimpleXsdSchema(new ClassPathResource("xsd/projectEndpoint.xsd"));
    }

    @Bean(name = "task")
    public DefaultWsdl11Definition taskWsdl11Definition(XsdSchema taskEndpointSchema) {
        final DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setServiceName("task");
        wsdl11Definition.setPortTypeName(TaskSoapEndpointImpl.PORT_TYPE_NAME);
        wsdl11Definition.setLocationUri(TaskSoapEndpointImpl.LOCATION_URI);
        wsdl11Definition.setTargetNamespace(TaskSoapEndpointImpl.NAMESPACE);
        wsdl11Definition.setSchema(taskEndpointSchema);
        return wsdl11Definition;
    }

    @Bean
    public XsdSchema taskEndpointSchema() {
        return new SimpleXsdSchema(new ClassPathResource("xsd/taskEndpoint.xsd"));
    }

    @Bean(name = "auth")
    public DefaultWsdl11Definition authWsdl11Definition(XsdSchema authEndpointSchema) {
        final DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setServiceName("auth");
        wsdl11Definition.setPortTypeName(AuthSoapEndpointImpl.PORT_TYPE_NAME);
        wsdl11Definition.setLocationUri(AuthSoapEndpointImpl.LOCATION_URI);
        wsdl11Definition.setTargetNamespace(AuthSoapEndpointImpl.NAMESPACE);
        wsdl11Definition.setSchema(authEndpointSchema);
        return wsdl11Definition;
    }

    @Bean
    public XsdSchema authEndpointSchema() {
        return new SimpleXsdSchema(new ClassPathResource("xsd/authEndpoint.xsd"));
    }

}
