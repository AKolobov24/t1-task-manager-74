package ru.t1.akolobov.tm.web.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import ru.t1.akolobov.tm.web.dto.Result;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class ServiceAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(
            final HttpServletRequest httpServletRequest,
            final HttpServletResponse httpServletResponse,
            final AuthenticationException e
    ) throws IOException {
        httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);

        @NotNull final PrintWriter writer = httpServletResponse.getWriter();
        @NotNull final ObjectMapper mapper = new ObjectMapper();

        @NotNull final Result result = new Result(e);
        @NotNull final String requestUri = httpServletRequest.getRequestURI();
        if (requestUri.startsWith("/api/"))
            writer.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(result));
        else
            httpServletResponse.sendRedirect("/login");
    }

}
